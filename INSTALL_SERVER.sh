#!/bin/bash
sudo apt-get -y install unison jq python3-virtualenv

if [ ! -d venv ]; then
  virtualenv -p python3 venv
fi

source venv/bin/activate
pip install -r requirements.txt

if [ -z config_s.json ]; then
  echo 'config_s.json not found, exiting!'
  exit 1
fi


if [ $(grep '"local_dir": "/local/dir",' config_s.json) ]; then
  echo 'Default configuration found! Modify config_s.json and re-run INSTALL_SERVER.sh. Exiting...'
  exit 1
fi

sed -e "s|WorkingDirectory=.*|WorkingDirectory=$(pwd)|" sync.service.sample > sync_s.service
sed -e "s|./sync.py X|./sync.py c|" sync_s.service > sync_s.service
sudo cp sync_s.service /etc/systemd/system/sync_s.service
sudo systemctl daemon-reload
sudo systemctl enable sync_s.service
sudo systemctl start sync_s.service

# if needed - generate certificate
# openssl req -new -x509 -days 365 -nodes -out server_cert.pem -keyout server_key.pem -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=example.com"