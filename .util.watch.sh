#!/bin/bash

LOCAL_DIR=`cat config_c.json | jq -r '.local_dir'`

watch -d -n0.5 "tree -Dsa ${LOCAL_DIR}"
