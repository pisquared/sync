# Sync Service (ala Dropbox)

NOTE: This project is derived from [unix_chat](https://gitlab.com/pisquared/unix_chat). The README.md and TODO will probably need to be transferred here.

Syncs a folder on your machine to a remote host via [unison](https://www.cis.upenn.edu/~bcpierce/unison/) and [systemd](https://freedesktop.org/wiki/Software/systemd/).

* As a *server* (`./sync.py s`) - it listens for connections on `0.0.0.0:<remote_port>`, adds connections as observers and notifies all observers over the established socket connection when a watched directory `remote_dir` changes
* As a *client* (`./sync.py s`) - it connects to a server at `<remote_host>:<remote_port>` to be notified via a socket connection in case `remote_dir` has changed. It also notifies and syncs if its `local_dir` has changed.

## Install
### Configure
`cp config_c.json.sample config_c.json` for client or `cp config_s.json.sample config_s.json` for server to set local, remote directory and remote connections.

If you don't have a certificate, on the server run:

```
openssl req -new -x509 -days 365 -nodes -out server_cert.pem -keyout server_key.pem -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=example.com"
```

Then you need to transfer `server_cert.pem` manually to all the clients.

### Install
Run `INSTALL_CLIENT.sh` or `INSTALL_SERVER.sh` (for Ubuntu based distros - take a look at the script or below for inspiration what is required on your distro).

### Project dependencies
* [python >= 3.5](https://www.python.org/) - using `async` methods and eventloops by watchgod
* [watchgod](https://pypi.org/project/watchgod/) - used to watch directories for changes [to be deprecated]
* [unison](https://www.cis.upenn.edu/~bcpierce/unison/) - file synchronizer
* [jq](https://stedolan.github.io/jq/) - read json from bash - used to parse configs from bash scripts [to be deprecated - the script doesn't do so much any more and could be replaced by in-python subprocess call]

## If you want encryption

This project doesn't set encryption but nothing stops you from doing it manually, for example - sync `~/.enc` folder having the encrypted files in `~/.enc`. If you want to use `encfs` for example:

```
# mount
encfs ~/.enc ~/Documents

# umount
fusermount -u ~/Documents
```
