#!/usr/bin/env python
import asyncio
import json
import os
import socket
import ssl
import subprocess
import sys
from time import sleep

import threading
from watchgod import awatch

EXPONENTIAL_BACKOFF = 1.2
EXPONENTIAL_BACKOFF_LIMIT = 30
HELP_USAGE = "\nUsage:\n    sync.py s - for server sync\n    sync.py c - for client sync"

THREADS = []
SOCKETS = []
OBSERVERS = {}
LOOPS = []


def notify_observers(originating=None, *args, **kwargs):
    print("NTO: Notifying {} observers...".format(len(OBSERVERS)))
    remove_observers = []
    for oid, observer in OBSERVERS.items():
        try:
            print("NTO: Notifying observer {}".format(oid))
            if not originating:
                originating = oid
            observer.send("{}".format(originating).encode())
        except BrokenPipeError:
            print("NTO: Observer has disconnected, setting to remove observer {}".format(oid))
            remove_observers.append(oid)
    for oid in remove_observers:
        del OBSERVERS[oid]
    print("NTO: Completed notifying observers.")


def send_update(*args, **kwargs):
    print("SNU: Sending update...")
    subprocess.call('./.sync.sh')
    print("SNU: Completed sending update...")


async def read_loop(method, dir):
    if not os.path.exists(dir):
        print("DIR: Local dir {} not existing - creating".format(dir))
        os.makedirs(dir)
    async for change in awatch(dir):
        print("DIR: Change detected - {}".format(change))
        method()
        print("DIR: Listening for changes...".format(change))


def listen():
    next_oid = 1
    backoff = 1
    host = '0.0.0.0'
    with open("config_s.json", "r") as config_f:
        config = json.load(config_f)

    port = config["remote_port"]
    server_key = config["server_key"]
    remote_cert = config["remote_cert"]

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    try:
        print("SRV: Trying to create socket {}:{}...".format(host, port))
        s = ssl.wrap_socket(s, keyfile=server_key, certfile=remote_cert, server_side=True)
    except FileNotFoundError as e:
        print("FATAL: SRV: Certificate for server not found. {}".format(e))
        os._exit(1)
    SOCKETS.append(s)
    while True:
        try:
            s.bind((host, port))
            break
        except OSError as e:
            print("SRV: Error trying to bind to socket: {}".format(e))
            print("SRV: Retrying in {:.1f} seconds".format(backoff))
            sleep(backoff)
            if backoff < EXPONENTIAL_BACKOFF_LIMIT:
                backoff *= EXPONENTIAL_BACKOFF
            continue
    print("SRV: Server started on port: %s" % port)
    s.listen(1)
    while True:
        print("SRV: Listening for connections...")
        try:
            conn, addr = s.accept()
        except OSError as e:
            print('SRV: Received OSError, assuming closed socket, breaking: {}'.format(e))
            break
        print('SRV: New connection from %s:%d' % (addr[0], addr[1]))
        print('SRV: Assigning observer id {}.'.format(next_oid))
        conn.send("oid:{}".format(next_oid).encode())
        OBSERVERS[next_oid] = conn
        next_oid += 1
        print('SRV: Added new observer.')


def connect():
    oid = False
    backoff = 1

    with open("config_c.json", "r") as config_f:
        config = json.load(config_f)

    host = config["remote_host"]
    port = config["remote_port"]
    remote_cert = config["remote_cert"]

    while True:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s = ssl.wrap_socket(s, cert_reqs=ssl.CERT_REQUIRED, ca_certs=remote_cert)
            print("CLI: Trying to connect {}:{}...".format(host, port))
        except FileNotFoundError as e:
            print("FATAL: CLI: Certificate for server not found. Provide {} file: {}".format(remote_cert, e))
            os._exit(1)
        try:
            s.connect((host, port))
        except (ConnectionRefusedError, OSError) as e:
            print("CLI: Error trying to connect: {}".format(e))
            print("CLI: Retrying in {:.1f} seconds".format(backoff))
            sleep(backoff)
            if backoff < EXPONENTIAL_BACKOFF_LIMIT:
                backoff *= EXPONENTIAL_BACKOFF
            continue
        backoff = 1
        print("CLI: Connected to {}:{}".format(host, port))
        while True:
            print("CLI: Listening for remote changes...")
            data = s.recv(1024)
            data = data.decode()
            print("CLI: Received data: {}".format(data))
            if data:
                if not oid:
                    print("CLI: No oid detected, assuming oid received".format(oid))
                    splitted = data.split(':')
                    if len(splitted) == 2:
                        oid = splitted[1]
                        print("CLI: Assigned oid {}".format(oid))
                # TODO: elif data != oid:
                print("CLI: Received changes: {}".format(data))
                send_update()
            else:
                print("CLI: Connection closed.")
                s.close()
                break


assert len(sys.argv) == 2, HELP_USAGE

command = sys.argv[1]


def async_read_loop(loop, *args):
    asyncio.set_event_loop(loop)
    LOOPS.append(loop)
    print('DIR: starting reading async loop...')
    try:
        loop.run_until_complete(read_loop(*args))
    except RuntimeError as e:
        print('DIR: stopping async loop... {}'.format(e))


def main():
    loop = asyncio.new_event_loop()
    if command in ["s", "server"]:
        with open("config_s.json", "r") as config_f:
            config = json.load(config_f)

        remote_dir = config["remote_dir"]

        t1 = threading.Thread(target=listen)
        t2 = threading.Thread(target=async_read_loop, args=(loop, notify_observers, remote_dir,))
        THREADS.append(('listen', t1))
        THREADS.append(('read', t2))
    elif command in ["c", "client"]:
        with open("config_c.json", "r") as config_f:
            config = json.load(config_f)

        local_dir = config["local_dir"]

        send_update()
        t1 = threading.Thread(target=connect)
        t2 = threading.Thread(target=async_read_loop, args=(loop, send_update, local_dir,))
        THREADS.append(('connect', t1))
        THREADS.append(('read', t2))
    else:
        print("incorrect arguments")
        exit(1)

    for thread in THREADS:
        thread[1].start()

    return command


def cleanup(command):
    if command in ["s", "server"]:
        print('CLN: closing {} observer sockets...'.format(len(OBSERVERS)))
        for i, observer in enumerate(OBSERVERS):
            print('CLN: closing observer socket {}...'.format(i))
            observer.close()
            print('CLN: closed observer socket {}.'.format(i))
        print('CLN: closing {} server socket...'.format(len(SOCKETS)))

        with open("config_s.json", "r") as config_f:
            config = json.load(config_f)

        remote_port = config["remote_port"]

        for i, s in enumerate(SOCKETS):
            print('CLN: closing server socket {}...'.format(i))
            s.shutdown(1)
            s.close()
            socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect(('localhost', remote_port))
            print('CLN: closed server sockets {}'.format(i))
        print('CLN: closing {} loops...'.format(len(LOOPS)))
    for i, l in enumerate(LOOPS):
        print('CLN: closing loop {}...'.format(i))
        l.stop()
        print('CLN: closed loop {}'.format(i))
    print('CLN: joining {} threads...'.format(len(THREADS)))
    for i, thread in enumerate(THREADS):
        print('CLN: joining thread {} - {}...'.format(i, thread[0]))
        thread[1].join()
        print('CLN: joined thread {}'.format(i))
    print('CLN: Done.')


if __name__ == '__main__':
    try:
        command = main()
        print("MAIN: Waiting for interruptions... Press Ctrl+C to cancel")
    except KeyboardInterrupt:
        print('MAIN: Interrupted')
        cleanup(command)
