#!/bin/bash

LOCAL_DIR=`cat config_c.json | jq -r '.local_dir'`
REMOTE_USER=`cat config_c.json | jq -r '.remote_user'`
REMOTE_HOST=`cat config_c.json | jq -r '.remote_host'`
REMOTE_DIR=`cat config_c.json | jq -r '.remote_dir'`

mkdir -p "${LOCAL_DIR}"

unison "${LOCAL_DIR}" "ssh://${REMOTE_USER}@${REMOTE_HOST}/${REMOTE_DIR}" -auto -batch -copyonconflict  -prefer newer -ignore "Path sensitive"
