#!/bin/bash
sudo apt-get -y install unison jq python3-virtualenv

if [ ! -d venv ]; then
  virtualenv -p python3 venv
fi

source venv/bin/activate
pip install -r requirements.txt

if [ -z config_c.json ]; then
  echo 'config_c.json not found, exiting!'
  exit 1
fi


if [ $(grep '"local_dir": "/local/dir",' config_c.json) ]; then
  echo 'Default configuration found! Modify config_c.json and re-run INSTALL_CLIENT.sh. Exiting...'
  exit 1
fi

sed -e "s|WorkingDirectory=.*|WorkingDirectory=$(pwd)|" sync.service.sample > sync_c.service
sed -e "s|./sync.py X|./sync.py c|" sync_c.service > sync_c.service
sudo cp sync_c.service /etc/systemd/system/sync_c.service
sudo systemctl daemon-reload
sudo systemctl enable sync_c.service
sudo systemctl start sync_c.service